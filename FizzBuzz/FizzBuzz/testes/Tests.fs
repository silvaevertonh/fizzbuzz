namespace testes

open System
open Microsoft.VisualStudio.TestTools.UnitTesting
open Libs

[<TestClass>]
type TestClass () =

    [<TestMethod>]
    member this.testComparaDivisaoPor3() = 
        let esperado = "Fizz\n"
        let teste = comparaDivisao 3
        Assert.AreEqual(esperado,teste)

    [<TestMethod>]
    member this.testComparaDivisaoPor5() = 
        let esperado = "Buzz\n"
        let teste = comparaDivisao 5
        Assert.AreEqual(esperado,teste)

    [<TestMethod>]
    member this.testComparaDivisaoPor3e5() = 
        let esperado = "FizzBuzz\n"
        let teste = comparaDivisao 15
        Assert.AreEqual(esperado,teste)

    [<TestMethod>]
    member this.testComparaIntervalo() = 
        let esperado = "\n"
        let teste = compararIntervalo "1 10"
        Assert.AreEqual(esperado,teste)