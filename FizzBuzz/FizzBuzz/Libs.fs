﻿module Libs
open System



let comparaDivisao numero =
    let mutable s = ""
    if (numero % 3) = 0 && (numero % 5) = 0 then
        s <- "FizzBuzz\n"
    elif (numero % 3) = 0 then
        s <- "Fizz\n"
    elif (numero % 5) =0 then
        s <- "Buzz\n"
    else
        s <- "-- " + numero.ToString() + " não é divisível por 3 e nem por 5!\n"
    
    s

let compararIntervalo s = 
    let t = "\n"
    let lista = s.ToString().Split(" ")
    let n1 = System.Int32.Parse(lista.[0])
    let n2 = System.Int32.Parse(lista.[1])
    
    for i in n1 .. n2 do
        printf "%s" (comparaDivisao i)
    
    t

let menu x = 
    match x with
    |    1 -> Console.Write("Digite um número: "), printf "%s" (comparaDivisao (System.Int32.Parse(Console.ReadLine())))
    |    2 -> Console.Write("Digite um intervalo de números separados por espaços: "), printf "%s" (compararIntervalo (Console.ReadLine()))

 